// ==UserScript==
// @name        Video Ad Replacer
// @description Support content creators while replacing video advertisements with pleasant alternatives.
// @match       *://*.youtube.com/*
// ==/UserScript==

/*
    @source: https://gist.github.com/jacob-willden/c0483aaf56ad48965ac2c9d0a1c350c4/
    @source: https://github.com/rdp/sensible-cinema/

    @licstart  The following is the entire license notice for the
    code in this page.

    This file is part of the Video Ad Replacer Extension Project.

    Video Ad Replacer Extension Project Copyright (C) 2021 Jacob Willden
    (Released under the GNU General Public License (GNU GPL) Version 3.0 
    or later)

    Sensible Cinema (Play It My Way) Source Code Copyright (C) 2016, 2017, 
    2018 Roger Pack 
    (Released under the Lesser General Public License (LGPL))

    Auto Skip YouTube Ads User Script Copyright (C) 2024 tientq64 (Tran Quang Tien)
    (Released under the MIT License (Expat Variation))

    Some of the code below is modified from the "edited_generic_player.js"
    source code file in the "chrome_extension" folder in the 
    "html5_javascript" folder from the Sensible Cinema (Play It My Way) 
    repository (source link above), and it is explicitly labled as so.

    One part of the code below is provided by wOxxOm from 
    StackOverflow, and is explicitly stated as so. Such code is 
    released under the Creative Commons Attribution Share-Alike 
    (CC BY-SA) 4.0. I specify Creative Commons as my proxy to 
    make the contributions from StackOverflow compatible with 
    future versions of the GPL.

    Afformentioned source code derived and modified by Jacob Willden
    Date of Derivation/Modification: November 5, 2021

    The Video Ad Replacer Extension Project is free software: 
    you can redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The project is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    the code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.

    You should have recieved a copy of the GNU General Public License
    along with this project. Otherwise, see: https://www.gnu.org/licenses/

    @licend  The above is the entire license notice for the 
    code in this page.
*/

'use strict';

/* Function derived and modified from "script.user.js" from Auto Skip
YouTube Ads, which is under the MIT License (Expat Varation):
Copyright (c) 2024 tientq64

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice (including the next paragraph) shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */
function skipIfPossible() {
    const skipButton = document.querySelector(`
        .ytp-skip-ad-button,
        .ytp-ad-skip-button,
        .ytp-ad-skip-button-modern
    `);
    if(skipButton) {
        skipButton.click();
    }
}

function replaceAds() {
    var myVideo = document.querySelector('video');
    var setForAdvertisement = false;
    
    // Function derived and modified from "edited_generic_player.js" from Sensible Cinema (checkStatus)
    function checkForAdvertisement() {
        var adIndicator = document.querySelector('.ytp-ad-player-overlay, .ytp-ad-button-icon');
        if(adIndicator) { // Ad is playing
            if(setForAdvertisement === false) {
                setForAdvertisement = true;
                myVideo.muted = true;
                myVideo.style.opacity = 0;
            }
            else {
                skipIfPossible();
            }
        }
        else { // Ad is over now
            if(setForAdvertisement === true) {
                setForAdvertisement = false;
                myVideo.muted = false;
                myVideo.style.opacity = '';
            }
        }
    }
    
    setInterval(checkForAdvertisement, 10); // Keep interval going in case there's another ad
}

replaceAds();

// Code below by wOxxOm at StackOverflow (CC BY-SA 4.0)
// https://stackoverflow.com/questions/34077641/how-to-detect-page-navigation-on-youtube-and-modify-its-appearance-seamlessly
document.body.addEventListener("yt-navigate-finish", function(event) {
    replaceAds();
});
